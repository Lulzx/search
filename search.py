import simdjson
import time

start = time.time()
with open('./result.json', 'rb') as fin:
    pj = simdjson.ParsedJson(fin.read())
    print(pj.items('.chats.list[0].name'))
    messages = pj.items('.chats.list[0].messages[]')

t = input("Enter search query: ")
indices = []
for i in range(len(messages)):
	text = str(messages[i]['text'])
	if t in text:
		indices.extend([messages[i]['id']])
print(indices)

print(len(messages))

end = time.time()

time_elapsed = end - start
print(time_elapsed)